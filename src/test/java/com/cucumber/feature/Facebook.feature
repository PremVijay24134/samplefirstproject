Feature: Facebook login functionality

Scenario: Facebook login with valid credential
Given User is on Facebook application page
When User enters valid username
And User enters valid password
And User click on the sign in button
Then User verify the username in the home page