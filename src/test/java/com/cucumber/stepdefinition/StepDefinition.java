package com.cucumber.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.cucumber.BaseClass.BaseClass;
import com.cucumber.runner.TestRunner;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinition extends BaseClass {
	public static WebDriver driver=TestRunner.driver;

@Given("^User is on Facebook application page")
public void user_is_on_Facebook_application_page()  {
          geturl("http://adactin.com/HotelApp/index.php");
}

@When("^User enters valid username")
public void user_enters_valid_username() {
WebElement username = driver.findElement(By.id("username"));
BaseClass.inputelement(username, "PremVijay");
}

@When("^User enters valid password")
public void user_enters_valid_password() {
	WebElement password = driver.findElement(By.xpath("//*[@id=\"password\"]"));
	BaseClass.inputelement(password, "prem@123");
	
}

@When("^User click on the sign in button")
public void user_click_on_the_sign_in_button() {
	WebElement loginbutton = driver.findElement(By.xpath("//*[@id=\"login\"]"));
	BaseClass.clickelement(loginbutton);
}

@Then("^User verify the username in the home page$")
public void user_verify_the_username_in_the_home_page() {
	WebElement verify = driver.findElement(By.id("input_show"));
	System.out.println(verify.getAttribute("value"));
	String expectedname="Hello PremVijay";
	if (verify.getAttribute("value").contains(expectedname)) {
		System.out.println("Valid User");
		}
	else {
		System.out.println("Invalid user");
	}
}

}